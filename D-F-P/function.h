#ifndef FUNCTION_H
#define FUNCTION_H
#include <math.h>
#include <iostream>

using namespace std;

double f(double x, double y);//функция

double f_x(double x, double y);//производная по х

double f_y(double x, double y);//производная по y

double step_k(double x, double y, double dx, double dy);

double mgrad(double x, double y);//модуль градиента

void end(double x, double y);//завершение работы вывод результата

#endif // FUNCTION_H

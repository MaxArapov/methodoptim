#include "function.h"

int main()
{
    double st_x = -1, st_y =-1, e=0.001;
    double x[25], y[25];
    x[0] = st_x;
    y[0] = st_y;
    double A[2][2];
    double dx =0, dy=0;
    double dgradx =0 , dgrady=0;
    A[0][0] = A[1][1] = 1;
    A[1][0] = A[0][1] = 0;
    int Mstep =25;
    int k = 0;
    double stepk;
    bool end_FL =false;
    cout<<"F(x,y) = x+y^2+((x+y-10)/3)^2"<<endl;
    cout<<"Start coordinate"<<'['<<st_x<<','<<st_y<<']'<<endl;
    while(end_FL ==false)
    {
        double dkx =0, dky=0;
        if(mgrad(x[k], y[k])<e)
        {
            end_FL = true;
            end(x[k], y[k]);
        }
        else
        {
            if(k>=Mstep)
            {
                end_FL = true;
                end(x[k], y[k]);
            }
            else
            {
                if(k==0)
                {
                    dkx = -(A[0][0]*f_x(x[k],y[k])+A[0][1]*f_y(x[k],y[k]));
                    dky = -(A[1][0]*f_x(x[k],y[k])+A[1][1]*f_y(x[k],y[k]));
                    stepk =floor((step_k(x[k],y[k],dkx*(-1),dky*(-1)))*1000)/1000;
                    x[k+1] =  floor((x[k] - stepk*(f_x(x[k],y[k])))*1000)/1000;
                    y[k+1] =  floor((y[k] - stepk*(f_y(x[k],y[k])))*1000)/1000;
                    cout<<"Iteration:"<<k+1<<endl;
                    cout<<"In coordinate:"<<'['<<x[k+1]<<','<<y[k+1]<<']'<<endl;
                    cout<<"Function:"<<f(x[k],y[k])<<endl;
                    if(fabs(x[k+1]-x[k])<e&&fabs(y[k+1]-y[k])<e&&fabs(f(x[k+1],y[k+1])-f(x[k],y[k]))<e)
                    {
                        end_FL = true;
                        end(x[k], y[k]);
                    }
                    else
                    {
                        k = k+1;
                    }
                }
                else
                {
                    double temp[2][2];
                    temp[0][0] =A[0][0];
                    temp[0][1] =A[0][1];
                    temp[1][0] =A[1][0];
                    temp[1][1] =A[1][1];
                    dgradx = f_x(x[k],y[k]) - f_x(x[k-1],y[k-1]);
                    dgrady = f_y(x[k],y[k]) - f_y(x[k-1],y[k-1]);
                    dx = x[k] - x[k-1];
                    dy = y[k] - y[k-1];
                    double At[2][2];
                    double z1 = dx*dgradx +dx*dgrady+dy*dgradx+dy*dgrady;
                    double z2 = dgradx*dgradx*A[0][0]+dgradx*dgrady*A[1][0]+dgradx*dgrady*A[0][1]+dgrady*dgrady*A[1][1];
                    At[0][0] = (dx*dgradx)/z1-(((A[0][0]*dgradx+A[0][1]*dgrady)*dgradx)*A[0][0]+((A[0][0]*dgradx+A[0][1]*dgrady)*dgrady)*A[1][0])/z2;
                    At[1][0] = (dy*dgradx)/z1-(((A[0][0]*dgradx+A[0][1]*dgrady)*dgradx)*A[0][1]+((A[0][0]*dgradx+A[0][1]*dgrady)*dgrady)*A[1][1])/z2;
                    At[0][1] = (dx*dgrady)/z1-(((A[1][0]*dgradx+A[1][1]*dgrady)*dgradx)*A[0][0]+((A[1][0]*dgradx+A[1][1]*dgrady)*dgrady)*A[1][0])/z2;
                    At[1][1] = (dy*dgrady)/z1-(((A[1][0]*dgradx+A[1][1]*dgrady)*dgradx)*A[0][1]+((A[1][0]*dgradx+A[1][1]*dgrady)*dgrady)*A[1][1])/z2;
                    A[0][0] = temp[0][0] - At[0][0];
                    A[0][1] = temp[0][1] - At[0][1];
                    A[1][0] = temp[1][0] - At[1][0];
                    A[1][1] = temp[1][1] - At[1][1];
                    dkx = -(A[0][0]*f_x(x[k],y[k])+A[0][1]*f_y(x[k],y[k]));
                    dky = -(A[1][0]*f_x(x[k],y[k])+A[1][1]*f_y(x[k],y[k]));
                    stepk =floor((step_k(x[k],y[k],dkx*(-1),dky*(-1)))*1000)/1000;
                    x[k+1] =  floor((x[k] - stepk*(dkx*(-1)))*1000)/1000;
                    y[k+1] =  floor((y[k] - stepk*(dky*(-1)))*1000)/1000;
                    cout<<"Iteration:"<<k+1<<endl;
                    cout<<"In coordinate:"<<'['<<x[k+1]<<','<<y[k+1]<<']'<<endl;
                    cout<<"Function:"<<f(x[k],y[k])<<endl;
                    if(fabs(x[k+1]-x[k])<e&&fabs(y[k+1]-y[k])<e)
                    {
                        if(fabs(f(x[k+1],y[k+1])-f(x[k],y[k]))<e)
                        {
                            end_FL = true;
                            end(x[k], y[k]);
                        }
                    }
                    else
                    {
                        k = k+1;
                    }
                }

            }



        }
    }
    system("pause");
}

#include "function.h"

using namespace std;

int main()
{
    double st_x = -1; double st_y =-1, e=0.01;
    int Mstep =100;
    int k = 0;
    double x[101], y[101], p_x[101], p_y[101], alpha[101], B[101], x_search = 0, y_search = 0;
    input(x,Mstep+1);
    input(y,Mstep+1);
    x[0] = 0;
    y[0] = 0;

    int N = 10;
    bool end_FL =false;

    cout<<"F(x,y) = x+y^2+((x+y-10)/3)^2"<<endl;
    cout<<"Start coordinate:"<<'('<<0<<','<<0<<')'<<endl;

    p_x[0] = - f_x(x[0], y[0]);
    p_y[0] = - f_y(x[0], y[0]);

    cout<<"e = "<<e<<endl;

    while (end_FL == false)
    {
        cout<<"Iteretion: "<<k<< endl;
        double p[2], X[2];
        p[0] = p_x[k];
        p[1] = p_y[k];


        X[0] = x[k];
        X[1] = y[k];

        cout<<"x["<<k<< "]="<<'('<<x[k]<<", "<<y[k]<<')'<<endl;
        cout<<"p["<<k<< "]=" <<'('<<p_x[k]<<", "<<p_y[k]<<')'<<endl;

        alpha[k] = FindMin(X, p);

        x[k+1] = x[k] + alpha[k] * p_x[k];
        y[k+1] = y[k] + alpha[k] * p_y[k];

        cout<<"a = "<<alpha[k]<< endl;
        cout<<"f'(x["<<k<< "]) = "<<'('<<f_x(x[k], y[k])<<", "<<f_y(x[k], y[k])<<')'<<endl;

        if (sqrt(pow(f_x(x[k+1], y[k+1]), 2) + pow(f_y(x[k+1], y[k+1]), 2)) < e)
        {
            x[0] = x[k+1];
            y[0] = y[k+1];
            end_FL = true;
            x_search = x[k+1];
            y_search = y[k+1];

            cout<<"||f'x[" << k+1 << "]|| = "<< sqrt(pow(f_x(x[k+1], y[k+1]), 2) + pow(f_y(x[k+1], y[k+1]), 2)) << endl;
            cout<<"||f'x[" << k+1 << "]|| < e"<< endl;
        }
        else
        {
            if ((k+1)%N == 0)
            {
                x[0] = x[k+1];
                y[0] = y[k+1];
                k = 0;
            }
            else
            {
                B[k] = pow(sqrt(pow(f_x(x[k+1], y[k+1]), 2) + pow(f_y(x[k+1], y[k+1]), 2)), 2) / pow(sqrt(pow(f_x(x[k], y[k]), 2) + pow(f_y(x[k], y[k]), 2)), 2);
                p_x[k+1] = -f_x(x[k+1], y[k+1]) + B[k]*p_x[k];
                p_y[k+1] = -f_y(x[k+1], y[k+1]) + B[k]*p_y[k];

                cout<<"B = "<<B[k]<<endl;
                cout<<"p["<<k+1<< "]="<<'('<<p_x[k+1]<<", "<<p_y[k+1]<<')'<<endl;

                k++;
            }
        }

        cout<<endl;
    }


    cout<<"x*"<<'('<<x_search<<", "<<y_search<<')'<<endl;


    system("pause");
}

#include <iostream>
#include <cstdlib>
#include <cmath>
#include "function.h"

//Одномерная оптимизация - применим метод золотого сечения
long double FindMin(double *s, double *p)
{
    const double eps = 1e-8;
    const double tay = 1.618;
    double a = 0;
    double b = 1e5;
    double x0, x1, xf1, xf2;
    x0 = b - (b - a) / tay; // Расчитываем точки деления
    x1 = a + (b - a) / tay;         //
P:
    double t1[2],t2[2];
    t1[0] = s[0] + x0 * p[0];
    t1[1] = s[1] + x0 * p[1];
    t2[0] = s[0] + x1 * p[0];
    t2[1] = s[1] + x1 * p[1];
    xf1 = f(t1[0], t1[1]); // Расчитываем в точках деления значение целевой функции
    xf2 = f(t2[0], t2[1]); //
    if(xf1 >= xf2)
    {
        a = x0;
        x0 = x1;
        t2[0] = s[0] + x1 * p[0];
        t2[1] = s[1] + x1 * p[1];
        xf1 = f(t2[0], t2[1]);
        t2[0] = s[0] + x1 * p[0];
        t2[1] = s[1] + x1 * p[1];
        x1 = a + (b - a) / tay;
        xf2 = f(t2[0], t2[1]);
    }
    else
    {
        b = x1;
        x1 = x0;
        xf2 = xf1;
        x0 = b - (b - a) / tay;
        t1[0] = s[0] + x0 * p[0];
        t1[1] = s[1] + x0 * p[1];
        xf1 = f(t1[0], t1[1]);
    }
    if(fabs(b - a) < eps)
    {
        return (a + b) / 2;
    }
    else
    goto P;
}

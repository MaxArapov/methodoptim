#ifndef FUNCTION_H
#define FUNCTION_H
#include <math.h>
#include <iostream>

using namespace std;

double f(double x, double y);//функция

double f_x(double x, double y);//производная по х

double f_y(double x, double y);//производная по y

double f_x_y();//2 производная по 2 м перемеенным

double f_x_x();//2 производная по x

double f_y_y();//2 производная по y

void input(double x[], int a);

double mgrad(double x, double y);//модуль градиента

void end(double x, double y);//завершение работы вывод результата

#endif // FUNCTION_H

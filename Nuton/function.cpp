#include "function.h"
double f(double x, double y)
{
    double res = x + pow(y,2) + pow((x+y-10.0)/3.0,2);

    return res ;
}

double f_x(double x, double y)
{
    double res = 1.0 +(2.0/9.0)*(x+y-10.0);

    return res;
}

double f_y(double x, double y)
{
    double res = 2.0*y +(2.0/9.0)*(x+y-10.0);

    return res;
}

double f_x_y()
{
    double res = 2.0/9.0;

    return res;
}

double f_x_x()
{
    double res = 2.0/9.0;

    return res;
}

double f_y_y()
{
    double res = 2.0+2.0/9.0;

    return res;
}

void input(double x[], int a)
{
    for(int i=0;i<a;i++)
    {
        x[i] =0;
    }
}

double mgrad(double x, double y)
{
    return sqrt(pow(f_x(x,y),2) + pow(f_y(x,y),2));
}

void end(double x, double y)
{
    double res1=0,res2=0,res3=0;
    res1 = x;
    res2 = y;
    res3 = f(x,y);
    cout<<"Min f(x,y):"<<res3<<endl;
    cout<<"In coordinate:"<<'['<<res1<<','<<res2<<']'<<endl;
}

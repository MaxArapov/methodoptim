#include "function.h"

using namespace std;

int main()
{
    double st_x = -1, st_y =-1, e=0.001;
    int Mstep =40;
    int k = 0;
    double x[41], y[41];
    x[0] = st_x;
    y[0] = st_y;
    input(x,Mstep+1);
    input(y,Mstep+1);
    double Gsm[2][2];//матрица Гессе
    double oGsm[2][2];//Минор матрицы Гессе
    Gsm[0][0] = f_x_x();
    Gsm[1][1] = f_y_y();
    Gsm[0][1]=Gsm[1][0]=f_x_y();
    double detG = (Gsm[0][0]*Gsm[1][1] - Gsm[0][1]*Gsm[1][0]);//Определитель матрицы Гессе
    oGsm[0][0] = Gsm[1][1];
    oGsm[1][1] = Gsm[0][0];
    oGsm[1][0] = -Gsm[0][1];
    oGsm[0][1] = -Gsm[1][0];
    bool end_FL =false;
    cout<<"F(x,y) = x+y^2+((x+y-10)/3)^2"<<endl;
    cout<<"Start coordinate"<<'['<<st_x<<','<<st_y<<']'<<endl;
    while(end_FL ==false)
    {
        if(mgrad(x[k], y[k])<e)
        {
            end_FL = true;
            end(x[k], y[k]);
        }
        else
        {
            if(k>=Mstep)
            {
                end_FL = true;
                end(x[k], y[k]);
            }
            else
            {
                double dfx = f_x(x[k],y[k]);
                double dfy = f_y(x[k],y[k]);
                if(oGsm[0][0]>0&&(oGsm[0][0]*oGsm[1][1]-oGsm[0][1]*oGsm[1][0])>0)//Проверка на положительность обратной матрицы Гессе
                {
                    x[k+1]= floor((x[k] -(detG*oGsm[0][0]*dfx+detG*oGsm[0][1]*dfy))*1000)/1000;
                    y[k+1]= floor((y[k] -(detG*oGsm[1][0]*dfx+detG*oGsm[1][1]*dfy))*1000)/1000;
                    cout<<"Iteration:"<<k+1<<endl;
                    cout<<"In coordinate:"<<'['<<x[k+1]<<','<<y[k+1]<<']'<<endl;
                    cout<<"Function:"<<f(x[k],y[k])<<endl;
                }
                else
                {
                    double t =0.5;
                    x[k+1]= floor((x[k] -t*f_x(x[k],y[k]))*1000)/1000;
                    y[k+1]= floor((y[k] -t*f_y(x[k],y[k]))*1000)/1000;
                    cout<<"Iteration:"<<k+1<<endl;
                    cout<<"In coordinate:"<<'['<<x[k+1]<<','<<y[k+1]<<']'<<endl;
                }
                if(fabs(x[k+1]-x[k])<e&&fabs(y[k+1]-y[k])<e&&fabs(f(x[k+1],y[k+1])-f(x[k],y[k]))<e)
                {
                    end_FL = true;
                    end(x[k], y[k]);
                }
                else
                {
                    k = k+1;
                }
            }
        }
    }
    system("pause");
}

#include "function.h"

using namespace std;

int main()
{
    double st_x = -1; double st_y =-1, e=0.001;
    int Mstep =100;
    int k = 0;
    double x[101], y[101];
    input(x,Mstep+1);
    input(y,Mstep+1);
    x[0] = st_x;
    y[0] = st_y;
    double step = 0.5;

    bool end_FL =false;
    cout<<"F(x,y) = x+y^2+((x+y-10)/3)^2"<<endl;
    cout<<"Start coordinate"<<'['<<st_x<<','<<st_y<<']'<<endl;
    while(end_FL ==false)
    {
        if(mgrad(x[k], y[k])<e)
        {
            end_FL = true;
            end(x[k], y[k]);
        }
        else
        {
            double t_step = step;
            if(k>=Mstep)
            {
                end_FL = true;
                end(x[k], y[k]);
            }
            else
            {
                x[k+1] = floor((x[k] - t_step*f_x(x[k],y[k]))*1000)/1000;
                y[k+1] = floor((y[k] - t_step*f_y(x[k],y[k]))*1000)/1000;
                cout<<"Iteration"<<k+1<<endl;
                cout<<"Coordinate':"<<'['<<x[k+1]<<','<<y[k+1]<<']'<<endl;
                cout<<"Function:"<<f(x[k],y[k])<<endl;
                if(fabs(x[k+1]-x[k])<e&&fabs(y[k+1]-y[k])<e&&fabs(f(x[k+1],y[k+1])-f(x[k],y[k]))<e)
                {
                    end_FL = true;
                    end(x[k], y[k]);
                }
                else
                {
                    k = k+1;
                }
            }
        }
    }
    system("pause");
}


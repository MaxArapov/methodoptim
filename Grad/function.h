#ifndef FUNCTION_H
#define FUNCTION_H
#include <math.h>
#include <iostream>

using namespace std;

double f(double x, double y);//функция

double f_x(double x, double y);//производная по х

double f_y(double x, double y);//производня по y

double mgrad(double x, double y);//модуль градиента

void end(double x, double y);//завершение работы вывод результата

void input(double x[], int a);

bool Check(double x[], double y[], int k);

#endif // FUNCTION_H
